TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        ../src/arguments.c \
        ../src/errors.c \
        ../src/lex.c \
        ../src/main.c \
        ../src/parser.c

HEADERS += \
    ../src/arguments.h \
    ../src/definitions.h \
    ../src/errors.h \
    ../src/lex.h \
    ../src/parser.h
